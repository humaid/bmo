<?php

function setImg($img){
 file_put_contents('img.txt', $img);
}

if(isset($_GET["set"])){
if($_GET["set"] == "sleeping"){
setImg("sleeping.gif");
} else if($_GET["set"] == "happyblink"){
setImg("happyblink.gif");
} else if($_GET["set"] == "happy"){
setImg("happy.png");
} else if($_GET["set"] == "blushing"){
setImg("blushing.gif");
} else if($_GET["set"] == "simplesmile"){
setImg("simplesmile.png");
} else if($_GET["set"] == "simplesmileblink"){
setImg("simplesmileblink.gif");
} else if($_GET["set"] == "worried"){
setImg("worried.png");
} else if($_GET["set"] == "straightface"){
setImg("straightface.png");
}

}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro|Source+Serif+Pro" rel="stylesheet">
    <link rel="stylesheet" href="https://humaidq.ae/css/main.css" media="all" />
    <style>
        body{
        
        
            font-family: 'Source Sans Pro', sans-serif;
        
          }
        h1, h2, h3, h4, h5, h6, .blog {
        
        font-family: 'Source Serif Pro', serif;
        }
        
    </style>
    <title>Wizard's Panel</title>
</head>

<body>
    <div class="container">
        <h1 class="title screen">Wizard's Panel</h1>
        <nav>
            <ul>
                <li class="active"><a href="">BMO</a></li>
            </ul>
        </nav>
        <div class="content">
                <h2 id="about-me">BMO Face Control >:)</h2>
<p>Current emotion: <?php include('img.txt'); ?></p>
<h4>Preview:</h4>
<img src="<?php include('img.txt'); ?>" height=100></img>
<h4>Set to:</h4>
<ul>
<li><a href="?set=sleeping">Sleeping</a></li>
<li><a href="?set=happyblink">Happy (with blink)</a></li>
<li><a href="?set=happy">Happy</a></li>
<li><a href="?set=simplesmile">Simple smile</a></li>
<li><a href="?set=simplesmileblink">Simple smile (with blink)</a></li>
<li><a href="?set=worried">Worried face</a></li>
<li><a href="?set=straightface">Straight face</a></li>
<li><a href="?set=blushing">Blushing face</a></li>
</ul>

        </div>
        <div class="footer">
            <p>Only wizards have the right to use this.</p>
            </div>
        </div>
        </body>
</html>
 

